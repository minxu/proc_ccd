#!c:\perl\bin\perl -w
#
# Min Xu
# split the multi-year csv data to yearly csv data


use strict;

#corn
#my $MYearDir='D:/MinXu/Research/Data_Center/Crop_County_Data/Corn/4grain/';
#my $SYearDir='D:/MinXu/Research/Data_Center/Crop_County_Data/Corn/4grain/CSVyearly/temp/';
#soybean
my $MYearDir='D:/MinXu/Research/Data_Center/Crop_County_Data/Soybean/';
my $SYearDir='D:/MinXu/Research/Data_Center/Crop_County_Data/Soybean/CSVyearly/temp/';

my (@files, $file, $line, $year, $ypre, $fout, $head);

@files = glob("$MYearDir/County_wo_units1960-2009.csv");


foreach $file (@files)
{
   print $file, "\n";
   open(FH, $file) || die "error in open file $file \n";
   $ypre = -9999.;
   $head = <FH>;
   while($line=<FH>)
   {
      $year = (split(/,/, $line))[2];

      if($year != $ypre && $ypre != -9999.)
      {
         close(FO);
      }

      if($year != $ypre)
      {
        $fout = "$SYearDir/County_wo_units$year.csv";
        open(FO, ">$fout");
        print FO $head;
      }
      else
      {
        print FO $line;
      }

      $ypre = $year;
   }
   close(FH);
}
