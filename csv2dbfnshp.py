#!C:\Python25\python.exe
import struct, datetime, decimal, itertools

def dbfreader(f):
    """Returns an iterator over records in a Xbase DBF file.

    The first row returned contains the field names.
    The second row contains field specs: (type, size, decimal places).
    Subsequent rows contain the data records.
    If a record is marked as deleted, it is skipped.

    File should be opened for binary reads.

    """
    # See DBF format spec at:
    #     http://www.pgts.com.au/download/public/xbase.htm#DBF_STRUCT

    numrec, lenheader = struct.unpack('<xxxxLH22x', f.read(32))    
    numfields = (lenheader - 33) // 32

    fields = []
    for fieldno in xrange(numfields):
        name, typ, size, deci = struct.unpack('<11sc4xBB14x', f.read(32))
        name = name.replace('\0', '')       # eliminate NULs from string   
        fields.append((name, typ, size, deci))
    yield [field[0] for field in fields]
    yield [tuple(field[1:]) for field in fields]

    terminator = f.read(1)
    assert terminator == '\r'

    fields.insert(0, ('DeletionFlag', 'C', 1, 0))
    fmt = ''.join(['%ds' % fieldinfo[2] for fieldinfo in fields])
    fmtsiz = struct.calcsize(fmt)
    for i in xrange(numrec):
        record = struct.unpack(fmt, f.read(fmtsiz))
        if record[0] != ' ':
            continue                        # deleted record
        result = []
        for (name, typ, size, deci), value in itertools.izip(fields, record):
            if name == 'DeletionFlag':
                continue
            if typ == "N":
                value = value.replace('\0', '').lstrip()
                if value == '':
                    value = 0
                elif deci:
                    value = decimal.Decimal(value)
                else:
                    value = int(value)
            elif typ == 'D':
                y, m, d = int(value[:4]), int(value[4:6]), int(value[6:8])
                value = datetime.date(y, m, d)
            elif typ == 'L':
                value = (value in 'YyTt' and 'T') or (value in 'NnFf' and 'F') or '?'
            elif typ == 'F':
                value = float(value)
            result.append(value)
        yield result


def dbfwriter(f, fieldnames, fieldspecs, records):
    """ Return a string suitable for writing directly to a binary dbf file.

    File f should be open for writing in a binary mode.

    Fieldnames should be no longer than ten characters and not include \x00.
    Fieldspecs are in the form (type, size, deci) where
        type is one of:
            C for ascii character data
            M for ascii character memo data (real memo fields not supported)
            D for datetime objects
            N for ints or decimal objects
            L for logical values 'T', 'F', or '?'
        size is the field width
        deci is the number of decimal places in the provided decimal object
    Records can be an iterable over the records (sequences of field values).
    
    """
    # header info
    ver = 3
    now = datetime.datetime.now()
    yr, mon, day = now.year-1900, now.month, now.day
    numrec = len(records)
    numfields = len(fieldspecs)
    lenheader = numfields * 32 + 33
    lenrecord = sum(field[1] for field in fieldspecs) + 1
    hdr = struct.pack('<BBBBLHH20x', ver, yr, mon, day, numrec, lenheader, lenrecord)
    f.write(hdr)
                      
    # field specs
    for name, (typ, size, deci) in itertools.izip(fieldnames, fieldspecs):
        name = name.ljust(11, '\x00')
        fld = struct.pack('<11sc4xBB14x', name, typ, size, deci)
        f.write(fld)

    # terminator
    f.write('\r')

    # records
    for record in records:
        f.write(' ')                        # deletion flag
        for (typ, size, deci), value in itertools.izip(fieldspecs, record):
            if typ == "N":
                value = str(value).rjust(size, ' ')
            elif typ == 'D':
                value = value.strftime('%Y%m%d')
            elif typ == 'L':
                value = str(value)[0].upper()
            else:
                value = str(value)[:size].ljust(size, ' ')
            assert len(value) == size
            f.write(value)

    # End of file
    f.write('\x1A')

if __name__ == '__main__':
    import os, sys, csv, glob, string, arcgisscripting
    from cStringIO import StringIO
    from operator import itemgetter
    import shutil

    # Firstly set the fieldspecs

    fieldchart = ('C', 9, 0)
    fieldinteg = ('N', 9, 0)
    fieldfloat = ('N',16, 6)
    
    fieldnames = ['COMMODITY', 'YEAR', 'STATE', 'COUNTY',
                  'STFIPS', 'DISTRICT', 'COFIPS', 'COMMCODE',
                  'IRR_PLT', 'NIR_PLT', 'TOT_PLT',
                  'IRR_HAV', 'NIR_HAV', 'TOT_HAV',
                  'IRR_YLD', 'NIR_YLD', 'TOT_YLD',
                  'IRR_PRD', 'NIR_PRD', 'TOT_PRD', 'STCO']
    
    fieldspecs = [fieldchart, fieldinteg, fieldchart, fieldchart,
                  fieldinteg, fieldinteg, fieldinteg, fieldinteg,
                  fieldfloat, fieldfloat, fieldfloat,
                  fieldfloat, fieldfloat, fieldfloat,
                  fieldfloat, fieldfloat, fieldfloat,
                  fieldfloat, fieldfloat, fieldfloat, ('C', 5, 0)]

    #poolcsv = glob.glob('D:/minxu/PrcCounty/US_Corn4Grain_CSV_DBF/xum*.csv')
    poolcsv = glob.glob('D:/minxu/PrcCounty/xum*.csv')




    #joinednames = "IRR_PLT; NIR_PLT; TOT_PLT; IRR_HAV; NIR_HAV; TOT_HAV; IRR_YLD; NIR_YLD; TOT_YLD; IRR_PRD; NIR_PRD; TOT_PRD"

    joinednames = string.join(fieldnames,';')

    origshp = glob.glob('D:/MinXu/PrcCounty/US_County_Map/CoUS_GCS07.*')
    gp = arcgisscripting.create(9.3)
    for filecsv in poolcsv:
        print 'Now formating ...'+filecsv

        filebase=os.path.splitext(os.path.basename(filecsv))[0]
        print fieldnames
        print fieldspecs

        fc = open(filecsv, 'rb')
        cv = csv.reader(fc)

        records = []
        for row in cv:
            record=[]
            stco = row[4].zfill(2)+row[6].zfill(3)
            record = row
            for i in range(8,19):
                #print float(row[i])
                record[i] = "%16.6f" % float(row[i])
            record.append(stco)
            records.append(record)
        fc.close()

        print len(fieldnames)
        filedbf = filebase+'_'+row[1]+'.dbf'
        fd = open(filedbf, 'wb')
        dbfwriter(fd, fieldnames, fieldspecs, records)
        fd.close()

        # prepare the shapefile
        for fs in origshp:
            fileext = os.path.splitext(os.path.basename(fs))[1]
            #fileshp = 'county_corn4grain_'+row[1]+fileext
            fileshp = 'county_soybean_'+row[1]+fileext
            shutil.copy (fs, fileshp)

        #fileshp = 'county_uplandcotton_'+row[1]+'.shp'
        fileshp = 'county_soybean_'+row[1]+'.shp'
        try:
            print filedbf, '==>', fileshp
            gp.joinfield(fileshp, "ATLAS_STCO", filedbf, "STCO", joinednames)
        except:
            print "Error"
            print gp.GetMessages(2)
