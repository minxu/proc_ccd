﻿#!c:\perl\bin\perl -w
use strict;

my @files;
my $file;
my @fnam;
my $fout;
my $line;
my @tems;

my @colnew;

my @linnew;
my @linplt;
my @linhav;
my @linyld;
my @linprd;

my $linprt;
my ($id, @ids);

my @splt;
my @shav;
my @cyld;
my @prod;

my $irrdir="D:/MinXu/Research/Data_Center/Irrmap_USDA/";
my @irrtyp=('Irrigated', 'Non Irrigated Total', 'Total For Crop');
my @irrfil=('table_cotton1987.csv','table_cotton1992.csv','table_cotton1997.csv');

my $rfil; my $irrg; my $cont; my $iy;


my (%irr1, %irr2, %irr3);

my $i=0;
foreach $rfil (@irrfil)
{
    $i++;
    open(FI, "$irrdir/$rfil") || die "Can't open file\n";
      <FI>;
      while($line = <FI>)
      {
          chop($line);
          #$line =~s/("\w*\s*),(\s*\w*")/$1_$2/g;
          $line=~s/("[\w\s]*),([\w\s]*")/$1_$2/g;;
          @tems = split /,/, $line;
          #print $i, $rfil, '===', $tems[0], '---', $tems[2], "\n";
          if($i==1) {$irr1{$tems[0]} = $tems[2];}  # unit is acre
          if($i==2) {$irr2{$tems[0]} = $tems[2];}
          if($i==3) {$irr3{$tems[0]} = $tems[2];}
      }
}

@files = glob("D:/MinXu/Research/Data_Center/Crop_County_Data/Cotton/Upland/*");

foreach $file (@files)
{
    if( -d $file )
    {
        # get the file name such as "County_wo_unitsXXXX.csv"
        @fnam = glob("$file/*wo*.csv");
        #print $fnam[0], "\n";
        
        $fout = "xum_".substr($fnam[0], -23);
        
        @linnew = ();
        @linplt = ();
        @linhav = ();
        @linyld = ();
        @linprd = ();
        
        @ids =();
        
        undef @splt;
        undef @shav;
        undef @cyld;
        undef @prod;
        
        open(FH, $fnam[0])|| die "can't open the file";
          <FH>;
          while($line = <FH>)
          {
              @colnew = ();
              @tems   = ();
              chomp($line);
              @tems = split(/,/, $line);
              next if ($tems[7] == 888 || $tems[7] == 999);
              
              $id = sprintf("%05d", $tems[5]*1000+$tems[7]);
              push (@ids, $id);
              
              $iy = $tems[2];

              if  ( $tems[1] eq $irrtyp[0] )
              {
                  $splt[$id]->[0] = $tems[10];
                  $shav[$id]->[0] = $tems[11];
                  $cyld[$id]->[0] = $tems[12];
                  $prod[$id]->[0] = $tems[13];
              }
              elsif( $tems[1] eq $irrtyp[1] )
              {
                  $splt[$id]->[1] = $tems[10];
                  $shav[$id]->[1] = $tems[11];
                  $cyld[$id]->[1] = $tems[12];
                  $prod[$id]->[1] = $tems[13];
              }
              elsif( $tems[1] eq $irrtyp[2] )
              {
                  $splt[$id]->[2] = $tems[10];
                  $shav[$id]->[2] = $tems[11];
                  $cyld[$id]->[2] = $tems[12];
                  $prod[$id]->[2] = $tems[13];
              }              
              else
              {
                  $splt[$id]->[0] = -9;
                  $shav[$id]->[0] = -9;
                  $cyld[$id]->[0] = -9;
                  $prod[$id]->[0] = -9;
                  $splt[$id]->[1] = -9;
                  $shav[$id]->[1] = -9;
                  $cyld[$id]->[1] = -9;
                  $prod[$id]->[1] = -9;
                  $splt[$id]->[2] = -9;
                  $shav[$id]->[2] = -9;
                  $cyld[$id]->[2] = -9;
                  $prod[$id]->[2] = -9;
              }
              
              # set value for [$id][0-2]
              $splt[$id]->[0] = -9 if(!defined($splt[$id]->[0])||$splt[$id]->[0]eq"");
              $shav[$id]->[0] = -9 if(!defined($shav[$id]->[0])||$shav[$id]->[0]eq"");
              $cyld[$id]->[0] = -9 if(!defined($cyld[$id]->[0])||$cyld[$id]->[0]eq"");
              $prod[$id]->[0] = -9 if(!defined($prod[$id]->[0])||$prod[$id]->[0]eq"");

              $splt[$id]->[1] = -9 if(!defined($splt[$id]->[1])||$splt[$id]->[1]eq"");
              $shav[$id]->[1] = -9 if(!defined($shav[$id]->[1])||$shav[$id]->[1]eq"");
              $cyld[$id]->[1] = -9 if(!defined($cyld[$id]->[1])||$cyld[$id]->[1]eq"");
              $prod[$id]->[1] = -9 if(!defined($prod[$id]->[1])||$prod[$id]->[1]eq"");

              $splt[$id]->[2] = -9 if(!defined($splt[$id]->[2])||$splt[$id]->[2]eq"");
              $shav[$id]->[2] = -9 if(!defined($shav[$id]->[2])||$shav[$id]->[2]eq"");
              $cyld[$id]->[2] = -9 if(!defined($cyld[$id]->[2])||$cyld[$id]->[2]eq"");
              $prod[$id]->[2] = -9 if(!defined($prod[$id]->[2])||$prod[$id]->[2]eq"");

              push (@colnew, ($tems[0], $tems[2], $tems[3], $tems[4]));
              push (@colnew, ($tems[5], $tems[6], $tems[7], $tems[8]));
              

              $linnew[$id] = join(',', @colnew);
          }
        close(FH);

        # remove the duplicated id
        my %temp = map {$_, 1} @ids;
        @ids = ();
        @ids = keys %temp;
                
        # check the consistence
        foreach $id (@ids)
        {      
            for ($i=0; $i<3; $i++)
            {
                if($shav[$id][$i]*$cyld[$id][$i]<0)
                {
                    print "$iy === $i -- Error $shav[$id][$i], $cyld[$id][$i]  \n";
                    $shav[$id][$i]=-9;
                    $cyld[$id][$i]=-9;
                }
            }            
        }
        &check_area(\@ids, \@splt, 'plt');   
        &check_area(\@ids, \@shav, 'hav');
        #&check_area(\@ids, \@cyld, 'yld');
        #&check_area(\@ids, \@prod, 'prd');
        open(FO, ">$fout");
        foreach $id (@ids)
        {
            next if($splt[$id][0] == -9 && $splt[$id][1] == -9 && $splt[$id][2] == -9);
            next if($shav[$id][0] == -9 && $shav[$id][1] == -9 && $shav[$id][2] == -9);
            next if($cyld[$id][0] == -9 && $cyld[$id][1] == -9 && $cyld[$id][2] == -9);
            next if($prod[$id][0] == -9 && $prod[$id][1] == -9 && $prod[$id][2] == -9);
            
            # for total for crop
            if( $shav[$id][0] == 0 && $shav[$id][1] == 0 && $shav[$id][2] != 0)
            {
                #print "$iy ($id) ==== $shav[$id][2] --- $irr1{$id}, $irr2{$id}, $irr3{$id} \n";
                if($iy <= 1987)
                {
                    if(defined $irr1{$id})
                    {
                        $irrg = $irr1{$id};
                    }
                    else
                    {
                        my $stid = int($id/1000);
                        if($stid == 6 || $stid == 4 || $stid == 35) # CA, AZ, NM
                        {
                            $irrg = $shav[$id][2];
                        }
                        else
                        {
                            $irrg = 0.;
                        }
                    }
                }
                # interploation !!!
                # -----------------
                if( $iy > 1987 && $iy <= 1992)
                {
                    if(defined $irr1{$id} && defined $irr2{$id})
                    {
                        $irrg = ($iy-1987)/5.*$irr2{$id} + 
                                (1992-$iy)/5.*$irr1{$id};
                    }
                    elsif(defined $irr1{$id})
                    {
                        $irrg = $irr1{$id};
                    }
                    elsif(defined $irr2{$id})
                    {
                        $irrg = $irr2{$id};
                    }
                    else
                    {
                        my $stid = int($id/1000);
                        if($stid == 6 || $stid == 4 || $stid == 35)
                        {
                            $irrg = $shav[$id][2];
                        }
                        else
                        {
                            $irrg = 0.;
                        }
                    }
                }
                if($iy > 1992 && $iy <= 1997)
                {
                    if(defined $irr2{$id} && defined $irr3{$id})
                    {
                        $irrg = ($iy-1992)/5.*$irr3{$id} + 
                                (1997-$iy)/5.*$irr2{$id};
                    }
                    elsif(defined $irr2{$id})
                    {
                        $irrg = $irr2{$id};
                    }
                    elsif(defined $irr3{$id})
                    {
                        $irrg = $irr3{$id};
                    }
                    else
                    {
                        my $stid = int($id/1000);
                        if($stid == 6 || $stid == 4 || $stid == 35)
                        {
                            $irrg = $shav[$id][2];
                        }
                        else
                        {
                            $irrg = 0.;
                        }
                    }
                }
                if($iy >= 1997)
                {
                    if(defined $irr3{$id})
                    {
                        $irrg = $irr3{$id};
                    }
                    else
                    {
                        my $stid = int($id/1000);
                        if($stid == 6 || $stid == 4 || $stid == 35) # CA, AZ, NM
                        {
                            $irrg = $shav[$id][2];
                        }
                        else
                        {
                            $irrg = 0.;
                        }
                    }
                }
                $shav[$id][0] = min($shav[$id][2], $irrg);
                $shav[$id][1] = max($shav[$id][2]-$shav[$id][0],0);                

                $cyld[$id][0] = $cyld[$id][2];
                $cyld[$id][1] = $cyld[$id][2];
            }
            #-------------------------------------------------------------------
            for( $i=2; $i<3; $i++)
            {
                if($cyld[$id][$i] <0)
                {
                    #print "$i, $shav[$id][$i], $cyld[$id][$i], $cyld[$id][0], $cyld[$id][1], $cyld[$id][2]\n";
                }
            }
           

            if( $cyld[$id][2] == -9 )
            {
                $cyld[$id][0] = max($cyld[$id][0], 0.0);
                $cyld[$id][1] = max($cyld[$id][1], 0.0);
                $cyld[$id][2] = max($cyld[$id][0], $cyld[$id][1]);
            }
            if( $cyld[$id][0] == -9 )
            {
                if( $shav[$id][0] == 0) 
                {
                    $cyld[$id][0] = 0.0;
                }
                else
                {
                    if($cyld[$id][1]==-9)
                    {
                        $cyld[$id][0]=max($cyld[$id][2], $cyld[$id][1]);
                        print "$iy xum $cyld[$id][0], $cyld[$id][1], $cyld[$id][2] \n";
                    }
                    else
                    {
                        $cyld[$id][0]=$cyld[$id][2]*$shav[$id][2]/$shav[$id][0]
                                     -$cyld[$id][1]*$shav[$id][1]/$shav[$id][0];
                        $cyld[$id][0]=max($cyld[$id][0], 0.0);
                    }
                    #exit;
                }
            }
            if( $cyld[$id][1] == -9 )
            {
                if( $shav[$id][1] == 0) 
                {
                    $cyld[$id][1] = 0.0;
                }
                else
                {
                    if($cyld[$id][0] == -9)
                    {
                        $cyld[$id][1]=max($cyld[$id][2], $cyld[$id][0]);
                        print "$iy xum $cyld[$id][0], $cyld[$id][1], $cyld[$id][2] \n";
                    }
                    else
                    {
                        $cyld[$id][1]=$cyld[$id][2]*$shav[$id][2]/$shav[$id][1]
                                     -$cyld[$id][0]*$shav[$id][0]/$shav[$id][1];
                        $cyld[$id][1]=max($cyld[$id][1], 0.0);
                    }
                    #exit;
                }
            }
            
            $linprt = join(',', ($linnew[$id],$splt[$id][0], $splt[$id][1], $splt[$id][2],
                                              $shav[$id][0], $shav[$id][1], $shav[$id][2],
                                              $cyld[$id][0], $cyld[$id][1], $cyld[$id][2],
                                              $prod[$id][0], $prod[$id][1], $prod[$id][2]));
                                              
            print FO $linprt, "\n";
       }    # station id
       close(FO);
    }
            #  exit if ($tems[2] == 1982 );
}

# make the three area to be consistent, first step consider non-irr
sub check_area
{
    my $jd; my $rat;
    my $rids = $_[0];
    my $rarr = $_[1];
    my $flag = $_[2];
    
    my @sids = @$rids;
    my @sarr = @$rarr;
    

    foreach $jd (@sids) 
    {
          if(    ($sarr[$jd][0] == -9 && $sarr[$jd][1] == -9) && $sarr[$jd][2] != -9)  # only total for crop
          {
              $sarr[$jd][0] = max($sarr[$jd][0], 0.0);
              $sarr[$jd][1] = max($sarr[$jd][1], 0.0);
          }          
          elsif( ($sarr[$jd][0] == -9 && $sarr[$jd][1] != -9) && $sarr[$jd][2] != -9)  # only non-irrigated
          {    
              $sarr[$jd][0] = max($sarr[$jd][2]-$sarr[$jd][1], 0.0);
          }
          elsif( ($sarr[$jd][0] != -9 && $sarr[$jd][1] == -9) && $sarr[$jd][2] != -9)  # only irrigated
          {
              $sarr[$jd][1] = max($sarr[$jd][2]-$sarr[$jd][0], 0.0);
          }
          elsif( ($sarr[$jd][0] != -9 && $sarr[$jd][1] != -9) && $sarr[$jd][2] != -9)  # only non-irrigated + irrigated
          {
              $sarr[$jd][0] = max($sarr[$jd][0], 0.0);
              $sarr[$jd][1] = max($sarr[$jd][1], 0.0);
              
              $rat = $sarr[$jd][0]/($sarr[$jd][0] + $sarr[$jd][1]);
              if( $sarr[$jd][2] > $sarr[$jd][0]+$sarr[$jd][1] )
              {
                  my $sadd      = $sarr[$jd][2]-$sarr[$jd][0]-$sarr[$jd][1];
                  $sarr[$jd][0] = $sarr[$jd][0]+$sadd *   $rat;
                  $sarr[$jd][1] = $sarr[$jd][1]+$sadd *(1-$rat);
              }
              $sarr[$jd][2] = $sarr[$jd][0]+$sarr[$jd][1];
          }
          elsif( ($sarr[$jd][0] == -9 && $sarr[$jd][1] == -9) && $sarr[$jd][2] == -9)
          {
              #print "$flag xum === $jd -- $tems[2] -- error \n";
              $sarr[$jd][0] = 0.;
              $sarr[$jd][1] = 0.;
              $sarr[$jd][2] = 0.;
          }
          elsif( ($sarr[$jd][0] != -9 || $sarr[$jd][1] != -9) && $sarr[$jd][2] == -9)
          {
              #print "$flag === test1, $sarr[$jd][0],  $sarr[$jd][1], $sarr[$jd][2], \n";
              $sarr[$jd][0] = max($sarr[$jd][0], 0.0);
              $sarr[$jd][1] = max($sarr[$jd][1], 0.0);        
              $sarr[$jd][2] = $sarr[$jd][0]+$sarr[$jd][1];
          }
          else
          {
              print "$flag === xxx \n";
          }
    }
}

sub max
{
    my $max = $_[0];
    for ( @_[ 1..$#_ ] ) 
    {
        $max = $_ if $_ > $max;
    }
    $max
}

sub min
{
    my $min = $_[0];
    for ( @_[ 1..$#_ ] ) 
    {
        $min = $_ if $_ < $min;
    }
    $min
}