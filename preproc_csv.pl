﻿#!c:\perl\bin\perl -w
################################################################################
# Min Xu
# ISWS
# 2009/07/07
# Changed from the perl script "irrg_add.pl"
# Main purpose: Preprocess the csv downloaded from USDA/NASS, seperate the 
#               irrigated and non-irrigated areas.

# 2009/11/06: check through the code, for corn, since currently we don't have
#             other irrigated inforamtion beside the crop county data (CCD), 
#             the t4c will be considered as nir, except the area is explicitly 
#             listed in the irr field in CCD
#             CA, AZ and NM always irrigated
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
use strict;

# setting crops

# cotton
#my $CsvDir = 'D:/MinXu/Research/Data_Center/Crop_County_Data/Cotton/Upland/';

# corn
#my $CsvDir = 'D:/MinXu/Research/Data_Center/Crop_County_Data/Corn/4grain/CSVyearly/';

# soybean
my $CsvDir = 'D:/MinXu/Research/Data_Center/Crop_County_Data/Soybean/CSVyearly/';



# file list will be processed
# one file instance,normally it is directory name
# file name in the directory
# output file name
my (@files, $file, @fnam, $fcsv, $fout, $line, @tems);    # file list will be processed

# irrigated type or practices
my @irrtyp=('Irrigated', 'Non Irrigated Total', 'Total For Crop');

my (@colnew, @linnew, $linprt);

my ($id, @ids);

# area planting for all purpose (P4AP)
# area harvesting (HARV)
# crop yield
# crop production
my (@splt, @shav, @cyld, @prod);

# this for cotton
my $irrdir="D:/MinXu/Research/Data_Center/Irrmap_USDA/";
my @irrfil=('table_cotton1987.csv','table_cotton1992.csv','table_cotton1997.csv');

my $rfil; my $irrg; my $cont; my $iy;
my (%irr1, %irr2, %irr3);
my $i=0;


undef %irr1;
undef %irr2;
undef %irr3;

=for "currently we don't have any ideas about irrigation information for corn"
foreach $rfil (@irrfil)
{
    $i++;
    open(FI, "$irrdir/$rfil") || die "Can't open file\n";
      <FI>;
      while($line = <FI>)
      {
          chop($line);
          #$line =~s/("\w*\s*),(\s*\w*")/$1_$2/g;
          $line=~s/("[\w\s]*),([\w\s]*")/$1_$2/g;;
          @tems = split /,/, $line;
          #print $i, $rfil, '===', $tems[0], '---', $tems[2], "\n";
          if($i==1) {$irr1{$tems[0]} = $tems[2];}  # unit is acre
          if($i==2) {$irr2{$tems[0]} = $tems[2];}
          if($i==3) {$irr3{$tems[0]} = $tems[2];}
      }
}
=cut


if($#ARGV > 0) 
{
   # interaction mode
   print "the interactive run, the comand agrument is the directory of csv file\n";
   @files = @ARGV;
}
else
{
   # batch mode
   print "using the batch run for multiple csv sub directory under $CsvDir\n";
   @files = glob("$CsvDir/*");
}


foreach $file (@files)
{
    if( -d $file )
    {
      # get the file name such as "County_wo_unitsXXXX.csv"
      @fnam = glob("$file/*wo*.csv");
      #print $fnam[0], "\n";

      foreach $fcsv (@fnam)
      {

        print "processing ... $fcsv \n";
        $fout = "xum_".substr($fcsv, -23);
        
        @linnew = ();
        
        @ids =();
        
        undef @splt;             # planted area [acre]
        undef @shav;             # havested area [acre]
        undef @cyld;             # yield [bushel/acre], around 56-58.7lb for corn, lb/ac for cotton
        undef @prod;             # production [bushel]
        
        # reading data ...

        open(FH, $fcsv)|| die "can't open the file";
          <FH>;    # skip the headline
          while($line = <FH>)
          {
              @colnew = ();
              @tems   = ();
              chomp($line);
              
              @tems = split(/,/, $line);

              next if($tems[7] == 888 || $tems[7] == 999);  # cycle if meet the district or state
              
              $id = sprintf("%05d", $tems[5]*1000+$tems[7]); # state id * 1000 + county id
              push (@ids, $id);                              # contain duplicated items
              
              $iy = $tems[2];

              foreach $i ((0, 1, 2))
              {
                 if( $tems[1] eq $irrtyp[$i] )   # irr
                 {
                     $splt[$id]->[$i] = $tems[10];
                     $shav[$id]->[$i] = $tems[11];
                     $cyld[$id]->[$i] = $tems[12];
                     $prod[$id]->[$i] = $tems[13];

                     # remove the space
                     $splt[$id]->[$i] =~ s/\s+//g;
                     $shav[$id]->[$i] =~ s/\s+//g;
                     $cyld[$id]->[$i] =~ s/\s+//g;
                     $prod[$id]->[$i] =~ s/\s+//g;
                 }


                 
                 # set value for [$id][0-2]
                 $splt[$id]->[$i] = -9 if(!defined($splt[$id]->[$i])||$splt[$id]->[$i]eq"");
                 $shav[$id]->[$i] = -9 if(!defined($shav[$id]->[$i])||$shav[$id]->[$i]eq"");
                 $cyld[$id]->[$i] = -9 if(!defined($cyld[$id]->[$i])||$cyld[$id]->[$i]eq"");
                 $prod[$id]->[$i] = -9 if(!defined($prod[$id]->[$i])||$prod[$id]->[$i]eq"");
              }

              # push some unchanged value to new column
              push (@colnew, ($tems[0], $tems[2], $tems[3], $tems[4]));
              push (@colnew, ($tems[5], $tems[6], $tems[7], $tems[8]));

              $linnew[$id] = join(',', @colnew);
          }
        close(FH);

        # remove the duplicated id
        my %temp = map {$_, 1} @ids;
        @ids = ();
        @ids = keys %temp;
                
        # check the areas
        &check_area(\@ids, \@splt, 'plt');   
        &check_area(\@ids, \@shav, 'hav');
        #&check_area(\@ids, \@cyld, 'yld');
        #&check_area(\@ids, \@prod, 'prd');

        open(FO, ">$fout");

        foreach $id (@ids)
        {
            # no data then cycle
            next if($splt[$id][0] == -9 && $splt[$id][1] == -9 && $splt[$id][2] == -9);
            next if($shav[$id][0] == -9 && $shav[$id][1] == -9 && $shav[$id][2] == -9);
            next if($cyld[$id][0] == -9 && $cyld[$id][1] == -9 && $cyld[$id][2] == -9);
            next if($prod[$id][0] == -9 && $prod[$id][1] == -9 && $prod[$id][2] == -9);


            # set zero on the check_area
            next if($splt[$id][0] ==  0 && $splt[$id][1] ==  0 && $splt[$id][2] ==  0);
            next if($shav[$id][0] ==  0 && $shav[$id][1] ==  0 && $shav[$id][2] ==  0);
            
            
            # for total for crop, the irrigated area will further identified if the other sources 
            # of irrigation information are defined in previous, otherwise, the area for total
            # for crop will be considered as a non-irrigated area.
            # confirm that in CA, AZ, NM, must irrigated

            if( $shav[$id][0] == 0 && $shav[$id][1] == 0 && $shav[$id][2] != 0)
            {
                #print "$iy ($id) ==== $shav[$id][2] --- $irr1{$id}, $irr2{$id}, $irr3{$id} \n";
                if($iy <= 1987)
                {
                    if(defined $irr1{$id})
                    {
                        $irrg = $irr1{$id};
                    }
                    else
                    {
                        my $stid = int($id/1000);
                        if($stid == 6 || $stid == 4 || $stid == 35) # CA, AZ, NM
                        {
                            $irrg = $shav[$id][2];
                        }
                        else
                        {
                            $irrg = 0.;
                        }
                    }
                }
                # interploation !!!
                # -----------------
                if( $iy > 1987 && $iy <= 1992)
                {
                    if(defined $irr1{$id} && defined $irr2{$id})
                    {
                        $irrg = ($iy-1987)/5.*$irr2{$id} + 
                                (1992-$iy)/5.*$irr1{$id};
                    }
                    elsif(defined $irr1{$id})
                    {
                        $irrg = $irr1{$id};
                    }
                    elsif(defined $irr2{$id})
                    {
                        $irrg = $irr2{$id};
                    }
                    else
                    {
                        my $stid = int($id/1000);
                        if($stid == 6 || $stid == 4 || $stid == 35)
                        {
                            $irrg = $shav[$id][2];
                        }
                        else
                        {
                            $irrg = 0.;
                        }
                    }
                }
                if($iy > 1992 && $iy <= 1997)
                {
                    if(defined $irr2{$id} && defined $irr3{$id})
                    {
                        $irrg = ($iy-1992)/5.*$irr3{$id} + 
                                (1997-$iy)/5.*$irr2{$id};
                    }
                    elsif(defined $irr2{$id})
                    {
                        $irrg = $irr2{$id};
                    }
                    elsif(defined $irr3{$id})
                    {
                        $irrg = $irr3{$id};
                    }
                    else
                    {
                        my $stid = int($id/1000);
                        if($stid == 6 || $stid == 4 || $stid == 35)
                        {
                            $irrg = $shav[$id][2];
                        }
                        else
                        {
                            $irrg = 0.;
                        }
                    }
                }
                if($iy >= 1997)
                {
                    if(defined $irr3{$id})
                    {
                        $irrg = $irr3{$id};
                    }
                    else
                    {
                        my $stid = int($id/1000);
                        if($stid == 6 || $stid == 4 || $stid == 35) # CA, AZ, NM
                        {
                            $irrg = $shav[$id][2];
                        }
                        else
                        {
                            $irrg = 0.;
                        }
                    }
                }
                $shav[$id][0] = min($shav[$id][2], $irrg);
                $shav[$id][1] = max($shav[$id][2]-$shav[$id][0], 0.);                

                #$cyld[$id][0] = $cyld[$id][2];
                #$cprd[$id][0] = $cyld[$id][0]*$shav[$id][0];

                #cyld[$id][1] = $cyld[$id][2];
                #cprd[$id][1] = $cyld[$id][1]*$shav[$id][1];
            }
            #-------------------------------------------------------------------
            # deal with P4AP based on the harvest fraction

            if( $splt[$id][0] == 0 && $splt[$id][1] == 0 && $splt[$id][2] != 0 )
            {

                if( $shav[$id][2] == 0 )
                { print "$shav[$id][0], $shav[$id][1], $shav[$id][2], \n"; }
                $splt[$id][0] = $splt[$id][2] * ($shav[$id][0] / $shav[$id][2]);
                $splt[$id][1] = $splt[$id][2] * ($shav[$id][1] / $shav[$id][2]);
            } 

            # now, all area has the values

            #if($id==46039)
            #{ print " xumdeb $shav[$id][0], $cyld[$id][0], $prod[$id][0], \n"; }

            # make cyld and cprd consitence, think cyld is correct if not consitence
            foreach $i ((0, 1, 2))
            {
                if( $shav[$id][$i] <= 1.0e-6 )
                {
                   $shav[$id][$i] = 0.;
                   $cyld[$id][$i] = 0.;
                   $prod[$id][$i] = 0.;
                }
                else
                {
                   if   ($cyld[$id][$i] != -9 && $prod[$id][$i] == -9) 
                      {$prod[$id][$i] = $cyld[$id][$i] * $shav[$id][$i];}
                   elsif($cyld[$id][$i] == -9 && $prod[$id][$i] != -9) 
                      {$cyld[$id][$i] = $prod[$id][$i] / $shav[$id][$i];}
                   elsif($cyld[$id][$i] != -9 && $prod[$id][$i] != -9) 
                      {$prod[$id][$i] = $cyld[$id][$i] * $shav[$id][$i];}
                   else
                      {;}
                }
            }

            # prod inconsistence in irr nir t4c
            if($prod[$id][0] != -9 && $prod[$id][1] != -9 && $prod[$id][2] != -9)
            {
                my $aaa = $prod[$id][2] - $prod[$id][1] - $prod[$id][0];

                if($aaa < 0)
                {
                   $prod[$id][2] = $prod[$id][1] + $prod[$id][0];
                }
                else
                {
                   my $prat = $prod[$id][0] / ($prod[$id][0] + $prod[$id][1]);
                   $prod[$id][0] = $prod[$id][0] + $aaa * $prat;
                   $prod[$id][1] = $prod[$id][1] + $aaa * (1.-$prat);
                   $prod[$id][2] = $prod[$id][1] + $prod[$id][0];
                }

                if($cyld[$id][0] != 0)
                  { $shav[$id][0] = $prod[$id][0] / $cyld[$id][0]; }
                if($cyld[$id][1] != 0)
                  { $shav[$id][1] = $prod[$id][1] / $cyld[$id][1]; }

                $shav[$id][2] = $shav[$id][0] + $shav[$id][1];
                $cyld[$id][2] = $prod[$id][2] / $shav[$id][2];
            }
             
            #if($id==46039)
            #{ print " xumdeb1 $shav[$id][0], $cyld[$id][0], $prod[$id][0], \n"; }

            if( $cyld[$id][2] == -9 )
            {
                $prod[$id][2] = max($prod[$id][0], 0.) + max($prod[$id][1], 0.);
                $cyld[$id][2] = $prod[$id][2] / $shav[$id][2];

                $prod[$id][0] = max($prod[$id][0], 0.);
                if($shav[$id][0] != 0)
                   { $cyld[$id][0] = max($prod[$id][0], 0.)/$shav[$id][0]; }

                $prod[$id][1] = max($prod[$id][1], 0.);
                if($shav[$id][1] != 0)
                   { $cyld[$id][1] = max($prod[$id][1], 0.)/$shav[$id][1]; }
            }

            #if($id==46039)
            #{ print " xumdeb2 ", $shav[$id][0]*$cyld[$id][0], " $prod[$id][0], \n"; }
            # if the total partiton to irr and nir, then we only know the total yield
            if( $cyld[$id][0] == -9 )
            {
                if( $cyld[$id][1] != -9 )
                {
                   $prod[$id][1] = min($prod[$id][2], $prod[$id][1]);
                   $prod[$id][1] = max($prod[$id][1], 0.);

                   # cyld[1] may equal aero if shav = 0
                   if( $shav[$id][1] != 0. )
                   {$cyld[$id][1] = max($prod[$id][1], 0.)/$shav[$id][1];}

                   $prod[$id][0] = max($prod[$id][2], 0.) - max($prod[$id][1], 0.);
                   $cyld[$id][0] = max($prod[$id][0], 0.)/$shav[$id][0];
                }
                else
                {
                   $cyld[$id][0] = $cyld[$id][2];
                   $prod[$id][0] = $cyld[$id][2]*$shav[$id][0];                  
                   $cyld[$id][1] = $cyld[$id][2];                   
                   $prod[$id][1] = $cyld[$id][2]*$shav[$id][1];                   
                }
            }
            else
            {
                if( $cyld[$id][1] != -9 )
                {
                   if($prod[$id][0] + $prod[$id][1] != $prod[$id][2])
                   {
                       print $prod[$id][0] + $prod[$id][1], "  $prod[$id][0], $prod[$id][1], $prod[$id][2], $id, \n";
                   }
                }
                else
                {
                   $prod[$id][0] = min($prod[$id][2], $prod[$id][0]);
                   $prod[$id][0] = max($prod[$id][0], 0.);

                   # cyld[1] may equal aero if shav = 0
                   if( $shav[$id][0] != 0. )
                   {$cyld[$id][0] = max($prod[$id][0], 0.)/$shav[$id][0];}

                   $prod[$id][1] = max($prod[$id][2], 0.) - max($prod[$id][0], 0.);
                   $cyld[$id][1] = max($prod[$id][1], 0.)/$shav[$id][1];
                }
            }

            if($shav[$id][2] != $shav[$id][0] + $shav[$id][1]) { print "shav $id\n"; }
            if($prod[$id][2] != $prod[$id][0] + $prod[$id][1]) { print "prod $id\n"; }

            if(($prod[$id][0] - $cyld[$id][0] * $shav[$id][0]) > 1.e-3) { print "irr  $id\n"; }
            if(($prod[$id][1] - $cyld[$id][1] * $shav[$id][1]) > 1.e-3) { print "nir  $id\n"; }
            if(($prod[$id][2] - $cyld[$id][2] * $shav[$id][2]) > 1.e-3) { print "t4c  $id\n"; }

            
            $linprt = join(',', ($linnew[$id],$splt[$id][0], $splt[$id][1], $splt[$id][2],
                                              $shav[$id][0], $shav[$id][1], $shav[$id][2],
                                              $cyld[$id][0], $cyld[$id][1], $cyld[$id][2],
                                              $prod[$id][0], $prod[$id][1], $prod[$id][2]));
                                              
            print FO $linprt, "\n";
        }    # station id
        close(FO);
      }      # fname
    }
            #  exit if ($tems[2] == 1982 );
}

# ==============================================================================
# make the three area to be consistent, first step consider non-irr
# ==============================================================================

sub check_area
{
    my $jd; my $rat;
    my $rids = $_[0];
    my $rarr = $_[1];
    my $flag = $_[2];
    
    my @sids = @$rids;
    my @sarr = @$rarr;
    
    foreach $jd (@sids) 
    {
          if(    ($sarr[$jd][0] == -9 && $sarr[$jd][1] == -9) && $sarr[$jd][2] != -9)  # only total for crop
          {
              $sarr[$jd][0] = max($sarr[$jd][0], 0.0);
              $sarr[$jd][1] = max($sarr[$jd][1], 0.0);
          }          
          elsif( ($sarr[$jd][0] == -9 && $sarr[$jd][1] != -9) && $sarr[$jd][2] != -9)  # only non-irrigated
          {    
              $sarr[$jd][1] = min($sarr[$jd][1], $sarr[$jd][2]); 
              $sarr[$jd][0] = max($sarr[$jd][2]-$sarr[$jd][1], 0.0);
          }
          elsif( ($sarr[$jd][0] != -9 && $sarr[$jd][1] == -9) && $sarr[$jd][2] != -9)  # only irrigated
          {
              $sarr[$jd][0] = min($sarr[$jd][0], $sarr[$jd][2]); 
              $sarr[$jd][1] = max($sarr[$jd][2]-$sarr[$jd][0], 0.0);
          }
          elsif( ($sarr[$jd][0] != -9 && $sarr[$jd][1] != -9) && $sarr[$jd][2] != -9)  # only non-irrigated + irrigated
          {
              $sarr[$jd][0] = max($sarr[$jd][0], 0.0);
              $sarr[$jd][1] = max($sarr[$jd][1], 0.0);
              
              if($sarr[$jd][0] + $sarr[$jd][1] != 0)
                 {$rat = $sarr[$jd][0]/($sarr[$jd][0] + $sarr[$jd][1]);}
              else
                 {$rat = 0.5;}
              if( $sarr[$jd][2] > $sarr[$jd][0]+$sarr[$jd][1] )
              {
                  my $sadd      = $sarr[$jd][2]-$sarr[$jd][0]-$sarr[$jd][1];
                  $sarr[$jd][0] = $sarr[$jd][0]+$sadd *   $rat;
                  $sarr[$jd][1] = $sarr[$jd][1]+$sadd *(1-$rat);
              }
              $sarr[$jd][2] = $sarr[$jd][0]+$sarr[$jd][1];
          }
          elsif( ($sarr[$jd][0] == -9 && $sarr[$jd][1] == -9) && $sarr[$jd][2] == -9)
          {
              #print "$flag xum === $jd -- $tems[2] -- error \n";
              $sarr[$jd][0] = 0.;
              $sarr[$jd][1] = 0.;
              $sarr[$jd][2] = 0.;
          }
          elsif( ($sarr[$jd][0] != -9 || $sarr[$jd][1] != -9) && $sarr[$jd][2] == -9)
          {
              #print "$flag === test1, $sarr[$jd][0],  $sarr[$jd][1], $sarr[$jd][2], \n";
              $sarr[$jd][0] = max($sarr[$jd][0], 0.0);
              $sarr[$jd][1] = max($sarr[$jd][1], 0.0);        
              $sarr[$jd][2] = $sarr[$jd][0]+$sarr[$jd][1];
          }
          else
          {
              print "$flag === xxx \n";
          }
    }
}

# maximum and minimum
sub max
{
    my $max = $_[0];
    for ( @_[ 1..$#_ ] ) 
    {
        $max = $_ if $_ > $max;
    }
    $max
}

sub min
{
    my $min = $_[0];
    for ( @_[ 1..$#_ ] ) 
    {
        $min = $_ if $_ < $min;
    }
    $min
}
